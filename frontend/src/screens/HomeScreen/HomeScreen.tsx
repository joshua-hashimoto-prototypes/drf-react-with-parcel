import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    IconButton,
} from "@material-ui/core";
import { Menu as MenuIcon } from "@material-ui/icons";
import axios from "axios";

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    })
);

const HomeScreen = () => {
    const classes = useStyles();
    const router = useHistory();

    const loadData = async () => {
        try {
            const credentials = {
                email: "example0@example.com",
                password: "Abcd1234",
            };
            const response = await axios.post(
                "http://localhost:8000/api/v1/auth/login/",
                credentials
            );
            console.log(response);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        loadData();
    }, []);

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        News
                    </Typography>
                    <Button
                        color="inherit"
                        onClick={() => {
                            router.push("/about");
                        }}>
                        About5
                    </Button>
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default HomeScreen;
