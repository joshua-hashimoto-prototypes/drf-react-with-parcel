import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import HomeScreen from "../screens/HomeScreen/HomeScreen";
import DetailScreen from "../screens/DetailScreen/DetailScreen";

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={HomeScreen} />
                <Route exact path="/about" component={DetailScreen} />
            </Switch>
        </BrowserRouter>
    );
};

export default Router;
