from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    # django admin
    path('headquarters/', admin.site.urls),
    # third party
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    path('api/v1/auth/registration/', include('dj_rest_auth.registration.urls')),
    path('api/v1/auth/', include('dj_rest_auth.urls')),
    # local apps
    path('api/v1/', include('page_api.urls')),
    path('', include('frontend.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
