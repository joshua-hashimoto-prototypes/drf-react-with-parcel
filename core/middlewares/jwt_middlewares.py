from django.conf import settings
from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin
import json


class JWTRefreshCookieToBodyMiddleware(MiddlewareMixin):

    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, *view_args, **view_kwargs):
        refresh_path = reverse('token_refresh')
        if request.path == refresh_path and settings.JWT_AUTH_REFRESH_COOKIE in request.COOKIES:
            if (request_body := request.body):
                data = json.loads(request_body)
                data['refresh'] = request.COOKIES[settings.JWT_AUTH_REFRESH_COOKIE]
                request._body = json.dumps(data).encode('utf-8')
        return None
