# django-docker-template

## Table of Content

[TOC]

---

## frontend

made frontend app using django's `startapp` to glue React to the project.

### Idea

The basic idea is the following;

1. use Percel to bundle React application,
2. On dev or production, Percel will render the bundled `index.js` to `frontend/static/frontned/` directory.
3. create `index.html` in `frontend/templates/frontend/`
4. `index.html` will react `index.js` in `frontend/static/frontend/` direcotry.
5. create a def view in `frontend/views.py` that renders the `index.html` and map it through `urls.py` in `core/` and `frontend`

### Framework/Library/Package/Tech

-   [Parcel](https://parceljs.org/)
-   [React](https://ja.reactjs.org/)
-   [TypeScript](https://www.typescriptlang.org/)
-   [Material UI](https://material-ui.com/)

### How it works

#### Step1: Frontend Django App

Create django app to contain frontend code(React).

```bash
~ % docker-compose exec web python manage.py startapp frontend
```

The following files are not needed in this django app.

-   admin.py
-   models.py
-   tests.py

#### Step2: Start NPM

Run the following command in frontend firecotry and start the npm project.

```sh
~ % cd frontend
~/fontend % npm init -y
```

#### Step3: Install basic packages for starting react project

For bundling we will use a package called [Parcel](https://parceljs.org/).

```sh
~/frontend % npm install --save-dev parcel@next @babel/core
~/frontned % npm install  @babel/preset-react
```

After the instalation is done, create `.babelrc` file under `frontned`.

```
frontend
├── __init__.py
├── .babelrc
├── apps.py
├── migrations
│   └── __init__.py
├── package-lock.json
├── package.json
└── views.py
```

Copy and paste the following code to `.babelrc`.

```json
{
    "presets": ["@babel/preset-react"]
}
```

Now we will configure `package.json`. The following change is for [Parcel](https://parceljs.org/) to work properlly with Django.

First we will change the `main` property to `app` and set the following value to it.

```json
{
    ~
    "app": "./static/frontend/index.js",
    ~
}
```

After that add the `targets` property. This is to include `node_modules` in bundled file. By default Parcel does not include `node_modules` to it's bundles in production. By setting this property Percel will include codes in `node_modules` to the production bundle.

```json
{
    ~
    "targets": {
        "app": {
            "includeNodeModules": true
        }
    },
    ~
}
```

Now we will write out scripts. Here we have 2 scripts, one for development and one for production.

```json
{
    ~
    "scripts": {
        "dev": "parcel ./src/index.jsx --dist-dir ./static/frontend/ --no-source-maps",
        "build": "parcel build ./src/index.jsx --dist-dir ./static/frontend/ --no-scope-hoist --no-source-maps --no-cache"
    },
    ~
}
```

Now it should look like this.

```json
{
    "name": "frontend",
    "version": "1.0.0",
    "description": "",
    "app": "./static/frontend/index.js",
    "targets": {
        "app": {
            "includeNodeModules": true
        }
    },
    "scripts": {
        "dev": "parcel ./src/index.tsx --dist-dir ./static/frontend/ --no-source-maps",
        "build": "parcel build ./src/index.tsx --dist-dir ./static/frontend/ --no-scope-hoist --no-source-maps --no-cache"
    },
    "keywords": [],
    "author": "",
    "license": "ISC",
    "dependencies": {
        "@babel/preset-react": "^7.12.13"
    },
    "devDependencies": {
        "@babel/core": "^7.13.8",
        "parcel": "^2.0.0-beta.1"
    }
}
```

#### Step4: Setup React

To start using React, first we need to install the necessary packages.

```sh
~/frontend % npm install react react-dom react-router-dom
```

After the installation is finished, make a `src` folder and create `index.jsx` and `App.jsx`.

```sh
~/frontend % mkdir src
~/frontend % touch src/App.jsx
~/frontend % touch src/index.jsx
```

```
frontend
├── __init__.py
├── .babelrc
├── apps.py
├── migrations
│   └── __init__.py
├── package-lock.json
├── package.json
├── src
│   ├── App.jsx
│   ├── index.jsx
└── views.py
```

Fill the new files with the following code.

`frontend/src/App.jsx`

```jsx
import React from "react";

const App = () => {
    return <div className="App">Hello Workd</div>;
};

export default App;
```

`frtonend/src/index.jsx`

```jsx
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(<App />, document.getElementById("app"));
```

#### Step5: Setup Django

Lastly, we will setup Django.

First, create `index.html` in `tempaltes/frontend/`.

```sh
~/frontend % mkdir templates
~/frontend % mkdir templates/frontend
~/frontend % touch templates/frontend/index.html
```

Copy and paste the following code to `tempaltes/frotnend/index.html`.

```html
{% load static %}
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
    </head>
    <body>
        <div id="main">
            <div id="app"></div>
        </div>

        <script src="{% static 'frontend/index.js' %}"></script>
    </body>
</html>
```

This `index.html` will be rendered through Django. When it renders it will read the `index.js` that will be outputed to `static/frontend`. This `index.js` file will be automatically created when running `npm run dev` or `npm run build`.

Create static folder for outputs.

```sh
~/frontend % mkdir static
~/frontend % mkdir static/frontend
```

Open `views.py` in `frontend` and create a def view to render the `index.html`.

`frontend/views.py`

```py
from django.shortcuts import render


def index(request, *args, **kwargs):
    return render(request, 'frontend/index.html')
```

Then, create `urls.py` and update the file.

```sh
~/frontend % touch urls.py
```

`frontend/urls.py`

```py
from django.urls import path

from .views import index


urlpatterns = [
    path('', index),
]
```

After that is done, open `core/urls.py` and connect the apps.

```py
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    # django admin
    path('headquarters/', admin.site.urls),
    # third party
    path('admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    path('auth/registration/', include('dj_rest_auth.registration.urls')),
    path('auth/', include('dj_rest_auth.urls')),
    # local apps
    path('api/v1/', include('page_api.urls')),
    path('', include('frontend.urls')), <- glue react frontend
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

```

#### Additinal1: TypeScript

When using Parcel for a bundler it is easy to use typescript.

First, install the necessary type files into your npm project.

```sh
~/frontend % npm install @types/react @types/react-dom @types/react-router-dom
```

Then, create `tsconfig.json` in the same directory as `package.json`.

```sh
~/frontend % touch tsconfig.json
```

Set your typescript configurations in this file. Parcel requires at least the following caonfiguration.

`frontend/tsconfig.json`

```json
{
    "compilerOptions": {
        "jsx": "react"
    }
}
```

After that change `.jsx` to `.tsx` and it should work. Don't forget to change the `.jsx` in `package.json`.

### Caveat

1. Every time you add a route in React we need to add the same route in `frontend/urls.py`. The path can return the same def view.

---

## backend

### Caveat

1. For token refresh, you need to send at lease an empty `{}` to the server for it to work.

---

## docker-compose.yml

```yml
version: "3"

services:
    web:
        build: .
        command: gunicorn core.wsgi -b 0.0.0.0:8000 # gunicorn対応
        environment:
            APPLICATION_NAME: django-docker-template
            ENVIRONMENT: development
            SECRET_KEY:
            DEBUG: 1
        volumes:
            - .:/app
        ports:
            - 8000:8000
        depends_on:
            - db
    db:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"

volumes:
    postgres_data:
```
